# funderingar

Funderingar om världen, människan, vi, dem och mig

## Lite om tekniken

Denhär README-filen beskriver innehållet i projektet funderingar.
Som man säger på svenska? "feel free att bidra med egna funderingar!"

Filen Funderingar.md innehåller den egentliga texten. Kommentera gärna och kom med sakliga ändringar.
Klicka på Commit efter dina tillägg så sparas ändringarna. Den förra versionen fins kvar så ändra och spara fritt!

Du kan gärna välja "Commit to main branch" efter att du tryckt på Commit
